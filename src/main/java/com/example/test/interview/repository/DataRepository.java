package com.example.test.interview.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.test.interview.model.Data;

@Repository
public interface DataRepository extends JpaRepository<Data , Long> {

    
}
