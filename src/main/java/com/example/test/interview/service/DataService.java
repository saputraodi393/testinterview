package com.example.test.interview.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.test.interview.model.Data;
import com.example.test.interview.repository.DataRepository;

@Service
public class DataService implements Serializable {


    @Autowired
    DataRepository dataRepository;

    public Data saveData( Data data){
        return dataRepository.save(data);
    }

    public List<Data> getDataAll(){
        return dataRepository.findAll();
    }


    
}
