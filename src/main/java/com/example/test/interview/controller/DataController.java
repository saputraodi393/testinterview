package com.example.test.interview.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.test.interview.model.Data;
import com.example.test.interview.resphonse.MessageResponse;
import com.example.test.interview.resphonse.ResphonseData;
import com.example.test.interview.service.DataService;



@RestController
@RequestMapping("/api/costumer")
public class DataController {

    @Autowired
    DataService dataService;


    @GetMapping("/get")
    public List<Data> getDataAllList(){
        return dataService.getDataAll();
    }
    
    @PostMapping("/post")
    public ResponseEntity<?> saveData(@RequestBody Data data){
        if(data.getName().equals(" ")){
            return  ResponseEntity.badRequest().body(new MessageResponse("Error: Nama harus diisi", "false"));
        }
        dataService.saveData(data);
        return ResponseEntity.ok(new ResphonseData(data.getId(), data.getName(), data.getAddres(), "true"));
    }
}
