package com.example.test.interview.resphonse;

public class ResphonseData {
    
    private String status;


    private Long id;
    private String name;
    private String addres;

    public ResphonseData( Long id, String name, String addres,String status) {
		this.id = id;
		this.name = name;
		this.addres = addres;
        this.status = status;

	}
    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddres() {
        return this.addres;
    }

    public void setAddres(String addres) {
        this.addres = addres;
    }
}
