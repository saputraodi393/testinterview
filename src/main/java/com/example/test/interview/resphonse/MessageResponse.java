package com.example.test.interview.resphonse;

public class MessageResponse {
    private String status;

    private String message;

	public MessageResponse(String message,String status) {
        this.status = status;
	    this.message = message;
	  }

      
    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
